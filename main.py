import asyncio
from nio import AsyncClient, RoomMessageText
from random import choice
import requests
import json

with open('settings.json') as settings_file:
    settings = json.load(settings_file)
    servers = settings['servers']

client = AsyncClient(settings['server'])

async def random_room(server):
    headers = {"Authorization": f"Bearer {settings['access_token']}"}
    r = requests.get(f"{settings['server']}/_matrix/client/v3/publicRooms?server={server}&limit=50", headers=headers)
    rooms = r.json()
    room = choice(rooms['chunk'])
    return room
    
async def send_message(room, response, event_id):
    print("sending a message")
            
    await client.room_send(
        room_id=room.room_id,
        message_type="m.room.message",
        content={
            "msgtype": "m.notice",
            "body": response,
            "m.relates_to": {
                "m.in_reply_to": {
                    "event_id": event_id
                }
            }
        }
    )   

async def on_message(room, event):
    try:
        if event.body == '!r':
            print("getting random room")
            current_server = choice(servers)
            print(current_server)
            result = await random_room(current_server)
            response = '\n'.join([
                f"Using {current_server} directory",
                f"Name: {result['name']}",
                f"Main alias: {result['canonical_alias']} ({result['room_id']})",
                f"Members: {result['num_joined_members']}"
            ])
            print(response)
            await send_message(room, response, event.event_id)
    except Exception as e:
        print(e)
        await send_message(room, "error", event.event_id)

async def main():
    print(client)
    client.access_token = settings["access_token"]
    client.user_id = settings["user_id"]
    print("syncing")
    await client.sync(timeout=5000)
    client.add_event_callback(on_message, RoomMessageText)
    print("syncing forever")
    await client.sync_forever(timeout=30000)
    
asyncio.get_event_loop().run_until_complete(main())
